
// survey 1

var ctx = document.getElementById('myChart1').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Male', 'Female', 'Other'], datasets: [{ label: 'My First dataset',backgroundColor: ["blue", "brown","orange"] , borderColor: 'white', data: [38.4,60.8,0.8] }] }, options: {} });


var ctx = document.getElementById('myChart2').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['ISC/ICSC', 'IGCSC/IB', 'CBSC', 'Other'],datasets: [{ label: 'My First dataset', backgroundColor:  ["blue", "orange","brown","green"], borderColor: 'white', data: [10.2,5.45, 78.9,5.45] }] }, options: {} });


var ctx = document.getElementById('myChart3').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['9th', '10th', '11th', '12th'], datasets: [{ label: 'My First dataset', backgroundColor: ["blue", "brown","orange","green"], borderColor: 'white', data: [14.4,14.4,52.8,18.4] }] }, options: {} });

var ctx = document.getElementById('myChart4').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Yes', 'No'], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown"], borderColor: 'white', data: [71.2,28.8 ] }] }, options: {} });

var ctx = document.getElementById('myChart5').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels:  ['Yes', 'No'], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown"], borderColor: 'white', data: [30.4,69.6] }] }, options: {} });

var ctx = document.getElementById('myChart6').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['0','1','2','3','4','5'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [0, 20,40,60] }] }, options: {} });

var ctx = document.getElementById('myChart7').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels:  ['0','1','2','3','4','5','6','7','8','9','10'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [0, 10, 5, 2, 20, 30, 45] }] }, options: {} });

var ctx = document.getElementById('myChart8').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels:  ['Yes', 'No'], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown"], borderColor: 'white', data: [60,40] }] }, options: {} });

var ctx = document.getElementById('myChart9').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['1','2','3','4','5','6','7','8','9','10'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [10,7.2,12,12,18.4,16,15,2,5.6,5.6,0,0] }] }, options: {} });

var ctx = document.getElementById('myChart10').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Yes', 'No'], datasets: [{ label: 'My First dataset', backgroundColor:  ["Blue","Brown"], borderColor: 'white', data: [45.6,54.4] }] }, options: {} });

var ctx = document.getElementById('myChart11').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Once a month', 'Every alternate month', 'Once a semister/term'], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown","green"], borderColor: 'white', data: [59.2,30.4,10.4] }] }, options: {} });

var ctx = document.getElementById('myChart12').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Yes', 'No', 'Maybe'], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown","orange"], borderColor: 'white', data: [29.6,20.8,49.6] }] }, options: {} });

var ctx = document.getElementById('myChart13').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Yes', 'No',], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown",], borderColor: 'white', data: [16,84] }] }, options: {} });

var ctx = document.getElementById('myChart14').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['1','2','3','4','5',], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [4,9.6,30.4,26.4,29.6] }] }, options: {} });

var ctx = document.getElementById('myChart15').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['1','2','3','4','5','6','7'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [6.4,16,25.6,28.8,23.2] }] }, options: {} });

var ctx = document.getElementById('myChart16').getContext('2d');
var chart = new Chart(ctx, { type: 'horizontalBar', data: { labels: ['Academic portion','Expectation of teachers','Expectation of parents','Compitativness of peers','Self expectations'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [57.6,39.2,57.6,50.4,72,8] }] }, options: {} });

var ctx = document.getElementById('myChart17').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Yes', 'No',], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown",], borderColor: 'white', data: [68,32] }] }, options: {} });

var ctx = document.getElementById('myChart18').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Yes', 'No','Occasionally'], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown","orange"], borderColor: 'white', data: [61.6,8.8,29.6] }] }, options: {} });

var ctx = document.getElementById('myChart19').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['1','2','3','4','5','6','7'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [7.6,9.5,10.4,12.1,10.3,6,37.9] }] }, options: {} });

var ctx = document.getElementById('myChart20').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['1','2','3','4','5','6','7','8','9'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [0.8,0,2.4,10.4,21.6,27.2,27.2,8.8,1.6] }] }, options: {} });

var ctx = document.getElementById('myChart21').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Studying/Homework','Entertainment','Social Networking','I get moe than 7 hours of sleep'], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown","green","purple",], borderColor: 'white', data: [41.6,17.6,20.8,20] }] }, options: {} });

var ctx = document.getElementById('myChart22').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Always','Often','Sometimes','Raraly','Never'], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown","green","purple","orange"], borderColor: 'white', data: [19.2,44,30.4,10.4] }] }, options: {} });

var ctx = document.getElementById('myChart23').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['0', '1','2','3'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [8,34.4,44,13.6] }] }, options: {} });

var ctx = document.getElementById('myChart24').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['0', '1','2','3'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [5.6,31.2,37.6,25.6] }] }, options: {} });





// survey 2

var ctx = document.getElementById('myChart25').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['ISC/ICSC', 'IGCSC/IB', 'CBSC', 'Other'],datasets: [{ label: 'My First dataset', backgroundColor:  ["blue", "orange","brown","green"], borderColor: 'white', data: [7.6,79.3,5,7.6] }] }, options: {} });


var ctx = document.getElementById('myChart26').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['9th', '10th', '11th', '12th'], datasets: [{ label: 'My First dataset', backgroundColor: ["blue", "brown","orange","green"], borderColor: 'white', data: [3.2,13.8,3.4,79.3] }] }, options: {} });


var ctx = document.getElementById('myChart27').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Yes', 'No',], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown",], borderColor: 'white', data: [29.3,20.7] }] }, options: {} });


var ctx = document.getElementById('myChart28').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['1','2','3','4','5',], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [17.2,13,8,17.2,20.7,31] }] }, options: {} });


var ctx = document.getElementById('myChart29').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Yes', 'No','Sometimes'], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown","orange"], borderColor: 'white', data: [58.6,6.9,34.5] }] }, options: {} });


var ctx = document.getElementById('myChart30').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['1','2','3','4','5'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [0,31.3,25.9,25.9,14.8] }] }, options: {} });


var ctx = document.getElementById('myChart31').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Yes', 'No',], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown",], borderColor: 'white', data: [89.7,10.3] }] }, options: {} });


var ctx = document.getElementById('myChart32').getContext('2d');
var chart = new Chart(ctx, { type: 'horizontalBar', data: { labels: ['Academic pressure','Expectation of teachers','Social pressure','Family/relationships','Lack of productivty','Thinking about future','Self expectations','When you know your good and capable enough'], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [65.5,34,3,37.9,58.6,55,2,3,4,3,4] }] }, options: {} });


var ctx = document.getElementById('myChart33').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['1','2','3','4','5',], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [55.2,17.2,17.2,3.4,6.9] }] }, options: {} });

var ctx = document.getElementById('myChart34').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Yes', 'No',], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown",], borderColor: 'white', data: [75.9,24.1] }] }, options: {} });



var ctx = document.getElementById('myChart35').getContext('2d');
var chart = new Chart(ctx, { type: 'pie', data: { labels: ['Always','Often','Sometimes','Raraly','Never'], datasets: [{ label: 'My First dataset', backgroundColor: ["Blue","Brown","green","purple","orange"], borderColor: 'white', data: [27.6,48.3,13.8,10.3] }] }, options: {} });


var ctx = document.getElementById('myChart36').getContext('2d');
var chart = new Chart(ctx, { type: 'bar', data: { labels: ['1','2','3','4','5',], datasets: [{ label: 'My First dataset', backgroundColor: 'purple', borderColor: 'white', data: [0,17.2,24.1,48.8,13.8] }] }, options: {} });